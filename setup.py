from setuptools import setup, find_packages

setup(
    name="debotnet",
    version="0.1.0",
    description="Distributed proxy system (Worker component)",
    url="https://gitlab.com/openpitpresents/debotnet/debotnet-client",
    license="GPLv3",
    author="Ave Ozkal, Luna Mendes, Eden Segal-Grossman",
    python_requires=">=3.8",
    install_requires=[
        "aiohttp==3.6.2",
        "websockets==8.1",
        "hail @ git+https://gitlab.com/elixire/hail.git@d72895019ef68eb96bb775f939182dd9344de36#egg=hail",
    ],
    packages=find_packages("src"),
    package_dir={"": "src"},
    py_modules=["debotnet_client"],
)
