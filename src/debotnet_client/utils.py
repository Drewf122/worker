# debotnet-client: Distributed proxy (Worker component)
# Copyright 2020, lavatech and debotnet-client contributors
# SPDX-License-Identifier: GPL-3.0-only

import socket


async def fetch_own_local_ip():
    # TODO find a way to do this with event loop?
    # copied from https://stackoverflow.com/a/28950776
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(("10.255.255.255", 1))
        ipdata = s.getsockname()
        return ipdata[0] if isinstance(ipdata, tuple) else ipdata
    except Exception:
        return "127.0.0.1"
    finally:
        s.close()
